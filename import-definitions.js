#!/usr/bin/env node

const colors = require('colors');

colors.enabled = true;

const argv = require('./yargs');
const api = require('./api.js');
const utils = require('./utils.js');
const objects = require('./objects.js');

// Imports a list of definitions from a CSV or XLSX file.

async function processDefinition(definitions) {
    await utils.forEach(definitions, async (definition) => {
        if (definition != null && !utils.isEmpty(definition.Name)) {
            console.log(`Evaluating ${definition.Name}`.grey);

            // We want serial execution
            // eslint-disable-next-line no-await-in-loop
            await objects.createOrUpdateDefinition(definition, {
                configureDefinition: async (/* apiDefinition, fromDefinition */) => {
                    // Adjust API definition object if necessary.
                },
            });
        }
    });

    done();
}

(async function runner() {
    const data = await run().catch((error) => fail(error));

    await processDefinition(data).catch((error) => fail(error));
}());

/*
 *********************************************************************************************************************
 Standard-ish component loaders, flow controllers, etc.
 *********************************************************************************************************************
*/

/**
 * Completes the importer script.
 */
function done() {
    console.log('Done'.brightWhite);
    process.exit(0);
}

/**
 * Completes the script with an error state.
 */
function fail(error) {
    if (error) {
        console.log('Failed:'.red);
        console.log(error);
    } else {
        console.log('Failed with unknown reason'.red);
    }
    process.exit(1);
}

/**
 * Standard importer setup and source loader.
 */
async function run() {
    api.authorize(argv.apikey, argv.apiroot);

    // Load the source file.
    return utils.loadSource(argv.input);
}
