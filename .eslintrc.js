module.exports = {
    root: true,
    extends: 'airbnb-base',
    env: {
        'browser': false,
    },
    'rules': {
        indent: ['error', 4, {
            SwitchCase: 1,
            VariableDeclarator: 1,
            outerIIFEBody: 1,
            MemberExpression: 0,
            FunctionDeclaration: {
                parameters: 1,
                body: 1
            },
            FunctionExpression: {
                parameters: 1,
                body: 1
            },
            CallExpression: {
                arguments: 1
            },
            ArrayExpression: 1,
            ObjectExpression: 1,
            ImportDeclaration: 1,
            flatTernaryExpressions: false,
            ignoredNodes: ['JSXElement', 'JSXElement *']
        }],
        "no-param-reassign": [2, {
            "props": false
        }],
        "prefer-template": 0,
        "max-len": ["error", 160],
        "comma-dangle": ['error', {
            arrays: 'always-multiline',
            objects: 'always-multiline',
            imports: 'always-multiline',
            exports: 'always-multiline',
            functions: 'ignore',
        }],
        "function-paren-newline": ["error", "consistent"],
        "import/no-extraneous-dependencies": 0,
        "no-console": 0,
        "prefer-destructuring": 0,
        "global-require": 0,
        "no-use-before-define": 0
    },
};
