/**
 * API helpers.
 */

const axios = require('axios');
const argv = require('yargs').argv;

const utils = require('./utils');

let apiKey = null;
let apiRoot = 'https://api.customizer.drivecommerce.com';

/**
 * Sets API key.
 * @param  {String} apiKey API key to use.
 * @param  {String} apiRoot API root URL.
 */
exports.authorize = function authorize(key, root) {
    apiKey = key;

    if (root && root.length > 0) {
        apiRoot = root;
    }
};

/**
 * Appends an API key as necessary.
 * @param  {String} url API call endpoint.
 * @return {String} API call URL with API key appended.
 */
function apiUrl(url) {
    if (apiKey == null || apiKey.length === 0) {
        throw new Error('API key is required');
    }

    const needSlash = !(url.startsWith('/') || apiRoot.endsWith('/'));

    const u = `${apiRoot}${needSlash ? '/' : ''}${url}`;

    if (u.indexOf('?') >= 0) {
        return `${u}&apikey=${apiKey}`;
    }

    return `${u}?apikey=${apiKey}`;
}

/**
 * Extracts errors list from Customizer validation message.
 */
function unpackError(error, url, type) {
    let response;

    if (error && error.response) {
        response = error.response.data;
    } else {
        response = error.data ? error.data : error;
    }

    const errors = [];

    if (response && response.modelState) {
        Object.values(response.modelState).forEach((message) => {
            if (message instanceof Array) {
                message.forEach((m) => {
                    errors.push(m);
                });
            } else {
                errors.push(message);
            }
        });
    }

    if (!errors.length) {
        if (response && response.exception) {
            errors.push(response.exception);
        }
    }

    if (!errors.length) {
        if (response && response.message) {
            errors.push(response.message);
        }
    }

    if (!errors.length) {
        if (error && error.response && error.response.status === 404) {
            errors.push('Not found');

            console.log(`${type} ${url}: Not found.`.dim.grey);
        } else {
            errors.push('Unexpected error, please try again in a few minutes');

            console.log(`${type} ${url}: Unexpected error, please try again in a few minutes.`.yellow);
        }
    } else {
        console.log(`${type} ${url} ${JSON.stringify(errors)}`.dim.red);
    }

    return errors;
}

/**
 * Sends a GET request to the API server.
 * @param  {String} url API call endpoint.
 * @return {Promise} Promise with call results.
 */
async function get(url) {
    if (argv.debug) {
        console.log(`GET ${url}`.dim.blue);
    }

    return axios.get(apiUrl(url)).catch((error) => {
        const errors = unpackError(error, url, 'GET');

        throw errors;
    });
}

exports.get = get;

/**
 * Sends a POST request to the API server.
 * @param  {String} url API call endpoint.
 * @param  {Object} data Data to be posted.
 * @return {Promise} Promise with call results.
 */
async function post(url, data) {
    if (argv.debug) {
        console.log(`POST ${url}`.dim.blue);
    }

    return axios.post(apiUrl(url), data).catch((error) => {
        const errors = unpackError(error, url, 'POST');

        throw errors;
    });
}

exports.post = post;

/**
 * Sends a PUT request to the API server.
 * @param  {String} url API call endpoint.
 * @param  {Object} data Data to be posted.
 * @return {Promise} Promise with call results.
 */
async function put(url, data) {
    if (argv.debug) {
        console.log(`PUT ${url}`.dim.blue);
    }

    return axios.put(apiUrl(url), data).catch((error) => {
        const errors = unpackError(error, url, 'PUT');

        throw errors;
    });
}

exports.put = put;

/**
 * Sends a PATCH request to the API server.
 * @param  {String} url API call endpoint.
 * @param  {Object} data Data to be posted.
 * @return {Promise} Promise with call results.
 */
async function patch(url, data) {
    if (argv.debug) {
        console.log(`PATCH ${url}`.dim.blue);
    }

    return axios.patch(apiUrl(url), data).catch((error) => {
        const errors = unpackError(error, url, 'PATCH');

        throw errors;
    });
}

exports.patch = patch;

/**
 * Sends a DELETE request to the API server.
 * @param  {String} url API call endpoint.
 * @param  {Object} data Data to be posted.
 * @return {Promise} Promise with call results.
 */
async function erase(url, data) {
    if (argv.debug) {
        console.log(`DELETE ${url}`.dim.blue);
    }

    return axios.delete(apiUrl(url), { data }).catch((error) => {
        const errors = unpackError(error, url, 'DELETE');

        throw errors;
    });
}

exports.erase = erase;

/**
 * Calls fnTrue or fnFalse callback depending on predicate results.
 * @param  {Function} predicate Predicate to check.
 * @param  {fnTrue} fn Callback to call if predicate resolves to true.
 * @param  {fnFalse} fn Callback to call if predicate resolves to false.
 * @return {Promise} Promise that resolves to a result of callbacks.
 */
exports.ifCondition = function ifCondition(predicate, fnTrue, fnFalse) {
    return new Promise((resolve, reject) => {
        if (predicate()) {
            fnTrue().then((v) => {
                resolve(v);
            }).catch((error) => {
                reject(error);
            });
        } else {
            fnFalse().then((v) => {
                resolve(v);
            }).catch((error) => {
                reject(error);
            });
        }
    });
};

/**
 * Helper method to check if a URL exists.
 * @param  {String} url A URL to check.
 * @return {Promise} Promise that is resolved or rejected based on URL existence.
 */
exports.ifExists = function ifExists(url) {
    return new Promise((resolve, reject) => {
        axios.head(url).then(() => {
            resolve();
        }).catch(() => {
            reject();
        });
    });
};

/**
 * Helper method to retrieve a product's latest version based on product ID.
 * @param  {String} productId Product ID.
 * @return {Promise} Promise with latest version data.
 */
async function getLatestVersion(productId) {
    const versions = await axios.get(apiUrl(`/api/v1/product/${productId}/version`));

    const latestVersion = versions.data[versions.data.length - 1];

    const versionUrl = `/api/v1/product/${productId}/version/${latestVersion.versionId}?expand=componentgroups%2Ccomponentplacements%2Cimageviews`;

    const version = await axios.get(apiUrl(versionUrl));

    return version.data;
}

exports.getLatestVersion = getLatestVersion;


const cachedStyleVersions = {};

/**
 * Helper method to retrieve a product's latest version based on product ID.
 * @param  {String} productId Product ID.
 * @return {Promise} Promise with latest version data.
 */
async function getCachedLatestVersionByStyle(styleCode) {
    if (cachedStyleVersions[styleCode]) {
        return cachedStyleVersions[styleCode];
    }

    const product = await axios.get(apiUrl(`/api/v1/product/style=${styleCode}`));

    const productId = product.data.productId;

    const version = await getLatestVersion(productId);

    cachedStyleVersions[styleCode] = version;

    return version;
}

exports.getCachedLatestVersionByStyle = getCachedLatestVersionByStyle;

/**
 * Helper method to retrieve a product's latest version based on product ID.
 * @param  {String} productId Product ID.
 * @return {Promise} Promise with latest version data.
 */
async function getLatestVersionByStyle(styleCode) {
    const product = await axios.get(apiUrl(`/api/v1/product/style=${styleCode}`));

    const productId = product.data.productId;

    return getLatestVersion(productId);
}

exports.getLatestVersionByStyle = getLatestVersionByStyle;


let cachedPlacements = null;

/**
 * Get placement by code.
 * @param  {String} code Placement code.
 * @return {Object} Found placement or null.
 */
async function getPlacementByCode(code) {
    if (cachedPlacements == null) {
        // Prevent chaining inside the method.
        const existingPlacementsResponse = await get('/api/v1/componentplacement');

        cachedPlacements = existingPlacementsResponse.data;
    }

    const existingPlacement = cachedPlacements.find((p) => p.code === code);

    return existingPlacement;
}

exports.getPlacementByCode = getPlacementByCode;

/**
 * Get version packages.
 */
async function getPackages(existingVersion) {
    const packages = (await get(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package`)).data;

    return packages;
}
exports.getPackages = getPackages;


const cachedGenerators = {};

/**
 * Retrieves version's named package generators.
 * @param  {Object} existingVersion Version object.
 * @param  {String} packageName Package name
 * @return {Object} Generators response
 */
async function getCachedGenerators(existingVersion, packageName) {
    const key = `${existingVersion.productId}::${existingVersion.versionId}::${packageName}`;

    if (cachedGenerators[key]) {
        return cachedGenerators[key];
    }

    let packages = (await get(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package`)).data;

    let generatorPackage = packages.componentPackages.find((p) => p.name === packageName);

    if (generatorPackage == null) {
        console.log(`... create package ${packageName}`.green);

        packages.componentPackages.push({
            name: packageName,
            temporaryId: utils.unique(),
            prices: [],
            customAttributesJson: [],
        });

        packages = (await put(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package`, packages)).data;

        generatorPackage = packages.componentPackages.find((p) => p.name === packageName);
    } else {
        console.log(`... Package ${packageName} already exists`.blue);
    }

    // eslint-disable-next-line max-len
    const generators = (await get(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package/${generatorPackage.componentPackageId}/generator`)).data;

    const result = {
        generatorPackage,
        generators,
    };

    cachedGenerators[key] = result;

    return result;
}

exports.getCachedGenerators = getCachedGenerators;

/**
 * Clears cached generators for a version.
 * @param  {Object} existingVersion Version object.
 * @param  {String} packageName Package name
 */
exports.clearCachedGenerators = function clearCachedGenerators(existingVersion, packageName) {
    const key = `${existingVersion.productId}::${existingVersion.versionId}::${packageName}`;

    delete cachedGenerators[key];
};

/**
 * Flushes the blueprint cache.
 */
async function flushCache() {
    await post('/api/v1/blueprint/flush', {});
}

exports.flushCache = flushCache;
