/**
 * Utilities.
 */

const fs = require('fs');
const moment = require('moment');

const argv = require('yargs/yargs')(process.argv.slice(2));

const uniquePrefix = new Date().getTime();
let uniqueSuffix = 0;

// Cause JavaScript is fun.
function isString(v) {
    return typeof v === 'string' || v instanceof String;
}

// Cleans up a data source.
function cleanUpData(data) {
    return data.map((row) => Object.fromEntries(Object.entries(row).map((v) => [
        isString(v[0]) ? v[0].trim() : v[0],
        isString(v[1]) ? v[1].trim() : v[1],
    ])));
}

/**
 * Loads the source file.
 * @return {Promise} Promise that is being resolved when the source is loaded.
 */
async function loadSource(source) {
    console.log(`Loading ${source}...`.grey);

    if (source.endsWith('.csv')) {
        // Load as csv.
        const csv = require('papaparse');

        const csvText = fs.readFileSync(source, 'utf8');

        const csvResult = csv.parse(csvText, {
            header: true,
        });

        return cleanUpData(csvResult.data);
    }

    if (source.endsWith('.xlsx')) {
        // Load as xslx.
        const XLSX = require('xlsx');

        const workbook = XLSX.readFile(source);

        const sheets = workbook.SheetNames;

        const imported = XLSX.utils.sheet_to_json(workbook.Sheets[sheets[0]], {
            raw: true,
            defval: null,
        });

        return cleanUpData(imported);
    }

    throw new Error('Unknown file format');
}

exports.loadSource = loadSource;

/**
 * Makes a unique value.
 */
function unique() {
    uniqueSuffix += 1;

    return `${uniquePrefix}-${uniqueSuffix}`;
}

exports.unique = unique;

/**
 * Updates tags list.
 *
 * Note: tags are never deleted, only added.
 */
exports.updateTags = function updateTags(to, from) {
    const list = to || [];

    if (from) {
        let tags = from;

        if (typeof tags === 'string') {
            tags = tags.split(',').map((tag) => tag.trim()).filter((tag) => tag.length > 0);
        }

        tags.forEach((tag) => {
            if (!list.includes(tag)) {
                list.push(tag);
            }
        });
    }

    return list;
};

/**
 * Internal custom attribute processor.
 */
function updateCustomAttributesInner(values, attributes, container, customPrefix, overwrite) {
    if (container) {
        Object.keys(container).forEach((key) => {
            if (key.startsWith(customPrefix)) {
                const name = key.substr(customPrefix.length);
                let value = container[key];

                if (value == null) {
                    value = '';
                } else {
                    value = value.toString().trim();
                }

                if (name.endsWith('-file') && value.length > 0) {
                    console.log(`    Reading '${value} file for ${name} attribute`.grey);

                    value = fs.readFileSync(value, 'utf8');
                }

                if (name.endsWith('-file.hidden') && value.length > 0) {
                    console.log(`    Reading '${value} file for ${name} attribute`.grey);

                    value = fs.readFileSync(value, 'utf8');
                }

                const existing = values.find((v) => v.name === name);
                if (existing) {
                    if (overwrite) {
                        existing.value = value;
                    }
                } else if (value && value.length > 0) {
                    values.push({
                        name,
                        value,
                        id: `id-${unique()}`,
                    });
                }
            }
        });
    }
}

/**
 * Updates the list of custom attributes.
 */
exports.updateCustomAttributes = function updateCustomAttributes(attributes, container, prefix) {
    const values = attributes || [];

    const customPrefix = prefix ? `${prefix} Custom ` : 'Custom ';

    updateCustomAttributesInner(values, attributes, container, customPrefix, true);

    const customInitialPrefix = prefix ? `${prefix} Initial Custom ` : 'Initial Custom ';

    updateCustomAttributesInner(values, attributes, container, customInitialPrefix, false);

    return values;
};

/**
 * Checks is CSV or XLSX is true-ish: 1, Yes, Y, True.
 */
function isTrue(v) {
    if (v != null) {
        const upV = v.toString().toUpperCase();

        return upV === '1' || upV === 'YES' || upV === 'Y' || upV === 'TRUE';
    }

    return false;
}

exports.isTrue = isTrue;

/**
 * Tests for empty objects. An object is considered if it is null, undefined, empty string, empty object, empty array.
 */
exports.isEmpty = function isEmpty(v) {
    if (v == null) {
        return true;
    }

    if (typeof v === 'string') {
        return v.trim().length === 0;
    }

    if (typeof v === 'object') {
        if (v instanceof Array) {
            return v.length === 0;
        }

        return Object.keys(v).length === 0 && v.constructor === Object;
    }

    return false;
};

/**
 * Checks if string is different.
 */
exports.isStringDifferent = function isStringDifferent(a, b) {
    return a !== (b == null ? b : b.toString());
};

/**
 * Checks if number is different.
 */
exports.isNumberDifferent = function isNumberDifferent(a, b) {
    return a !== (b == null ? b : parseFloat(b.toString()));
};

/**
 * Checks if boolean is different.
 */
exports.isBooleanDifferent = function isBooleanDifferent(a, b) {
    return a !== isTrue(b);
};

/**
 * Checks if boolean is different.
 */
exports.isBooleanNotDifferent = function isBooleanNotDifferent(a, b) {
    return a !== (!isTrue(b));
};

/**
 * Removes a property recursively.
 */
function removeProperty(o, ...list) {
    if (o != null) {
        if (Array.isArray(o)) {
            o.forEach((v) => removeProperty(v, ...list));
        } else {
            Object.keys(o).forEach((property) => {
                if (list.includes(property)) {
                    delete o[property];
                } else if (Array.isArray(o[property])) {
                    removeProperty(o[property], ...list);
                } else if (typeof o[property] === 'object') {
                    removeProperty(o[property], ...list);
                }
            });
        }
    }

    return o;
}

exports.removeProperty = removeProperty;

/**
 * Captures an object copy without specified properties.
 */
function captureObject(o, ...block) {
    return JSON.stringify(removeProperty(JSON.parse(JSON.stringify(o)), ...block));
}

exports.captureObject = captureObject;

/**
 * Checks if objects are different.
 */
exports.isObjectDifferent = function isObjectDifferent(a, before, ...block) {
    const now = captureObject(a, ...block);

    return now !== before;
};

/**
 * Converts a value to string.
 */
exports.asString = function asString(v) {
    return v == null ? v : v.toString();
};

/**
 * Converts a value to number.
 */
exports.asNumber = function asNumber(v) {
    return v == null ? v : parseFloat(v.toString());
};

/**
 * Queues up a microtask.
 */
exports.next = function next(fn) {
    queueMicrotask(fn);
};

/**
 * Parse a date value taking a custom date format into the account.
 */
exports.parseDate = function parseDate(value) {
    const date = argv.dateformat ? moment(value, argv.dateformat) : moment(value);

    if (date.isValid()) {
        return date;
    }

    // Bail on bad values.

    console.log(`Invalid date '${value}'`.red);
    process.exit(1);

    return null;
};

/**
 * Async forEach helper.
 */
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index += 1) {
        // We want serial execution
        // eslint-disable-next-line no-await-in-loop
        await callback(array[index], index, array);
    }
}

exports.forEach = asyncForEach;
