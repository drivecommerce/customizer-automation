const api = require('./api.js');
const utils = require('./utils.js');

/**
 * Updates tags.
 */
function updateTags(to, from) {
    Object.keys(from).forEach((tagKey) => {
        if (tagKey.startsWith('Delete Tags')) {
            let tags = from[tagKey];

            if (tags) {
                tags = tags.split(',').map((x) => x.trim()).filter((x) => x.length > 0);

                if (to.tags) {
                    to.tags = to.tags.filter((t) => !tags.includes(t));
                }
            }
        }
    });

    Object.keys(from).forEach((tagKey) => {
        if (tagKey.startsWith('Tags')) {
            to.tags = utils.updateTags(to.tags, from[tagKey]);
        }
    });
}

/**
 * Populates API definition object from a CSV source..
 */
async function updateDefinition(to, from) {
    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    if (!utils.isEmpty(from['Tracks Inventory'])) {
        to.tracksInventory = utils.isTrue(from['Tracks Inventory']);
    }

    updateTags(to, from);

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);
}

/**
 * Populates API option object from a CSV source..
 */
async function updateOption(to, from, definitions) {
    const before = utils.captureObject(to);

    if (!utils.isEmpty(from.Definition)) {
        const definition = definitions.find((d) => d.name === from.Definition);

        if (definition == null) {
            throw new Error(`Could not find component option definition '${from.Definition}'`);
        }

        to.componentOptionDefinitionId = definition.componentOptionDefinitionId;
    }

    if (!utils.isEmpty(from.Code)) {
        to.code = utils.asString(from.Code);
    }

    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    if (!utils.isEmpty(from.Active)) {
        to.inactive = !utils.isTrue(from.Active);
    }

    if (!utils.isEmpty(from['Active From'])) {
        to.activeFrom = utils.parseDate(from['Active From']).toISOString();
    }

    if (!utils.isEmpty(from.Placeholder)) {
        to.placeholder = utils.isTrue(from.Placeholder);
    }

    if (!utils.isEmpty(from['Placeholder Optional'])) {
        to.placeholderOptional = utils.isTrue(from['Placeholder Optional']);
    }

    if (!utils.isEmpty(from.Preferred)) {
        to.preferred = utils.isTrue(from.Preferred);
    }

    if (!utils.isEmpty(from['Order Key'])) {
        to.explicitOrder = utils.asNumber(from['Order Key']);
    }

    if (!utils.isEmpty(from.Multiplier)) {
        to.quantityMultiplier = utils.asNumber(from.Multiplier);
    }

    updateTags(to, from);

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);

    // Update dimensions.
    const dimensions = to.dimensionsJson || [];
    Object.keys(from).filter((key) => key.startsWith('Dimension')).forEach((dimension) => {
        const key = dimension.substr('Dimension'.length).trim();

        const existing = dimensions.find((d) => d.code === key);

        if (existing) {
            existing.value = (from[dimension] || '').toString().trim();
        } else {
            dimensions.push({
                code: key,
                value: (from[dimension] || '').toString().trim(),
            });
        }
    });

    to.dimensionsJson = dimensions;

    return utils.isObjectDifferent(to, before);
}

/**
 * Populates API component type object from a CSV source..
 */
async function updateType(to, from, definitions) {
    const before = utils.captureObject(to);

    if (!utils.isEmpty(from.Code)) {
        to.code = utils.asString(from.Code);
    }

    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    updateTags(to, from);

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);

    const definitionAssociations = [];

    for (let definitionIndex = 1; ; definitionIndex += 1) {
        const attributeName = `Definition ${definitionIndex}`;

        if (!utils.isEmpty(from[attributeName])) {
            const definition = definitions.find((d) => d.name === from[attributeName]);

            if (definition == null) {
                throw new Error(`Could not find component option definition '${from[attributeName]}'`);
            }

            definitionAssociations.push({
                componentOptionDefinitionId: definition.componentOptionDefinitionId,
            });
        } else {
            break;
        }
    }

    to.componentOptionDefinitions = definitionAssociations;

    const formats = [];

    for (let formatIndex = 1; ; formatIndex += 1) {
        const attributeName = `Format ${formatIndex}`;

        if (!utils.isEmpty(from[attributeName])) {
            const value = from[attributeName];

            const formatMatch = value.match(/([A-Za-z0-9_\-. ]+)(?:\((?:(\d+)(?: "([A-Za-z0-9_\-. ])")?)\))?\s+of\s+([A-Za-z0-9_\-. ]+)/);

            if (formatMatch && formatMatch.length === 5) {
                const attribute = formatMatch[1];
                const fieldWidth = formatMatch[2];
                const fieldFill = formatMatch[3];
                const relatedDefinition = formatMatch[4];

                const definition = definitions.find((d) => d.name === relatedDefinition);

                if (definition == null) {
                    throw new Error(`Could not find component option definition '${relatedDefinition}' when building part code format`);
                }

                if (formatMatch[1] === 'code') {
                    formats.push({
                        name: relatedDefinition,
                        targetId: definition.componentOptionDefinitionId,
                        type: 'code',
                    });
                } else {
                    formats.push({
                        name: relatedDefinition,
                        targetId: definition.componentOptionDefinitionId,
                        type: 'attribute',
                        attribute,
                        fill: fieldFill,
                        width: +fieldWidth,
                    });
                }
            } else {
                formats.push({
                    text: value,
                    type: 'static',
                });
            }
        } else {
            break;
        }
    }

    to.partCodeFormat = formats;

    return utils.isObjectDifferent(to, before);
}

/**
 * Populates API placement object from a CSV source..
 */
async function updatePlacement(to, from, types) {
    const before = utils.captureObject(to);

    const associatedTypes = [];

    Object.keys(from).forEach((column) => {
        if (!utils.isEmpty(column) && /^Type\s+\d+$/.test(column)) {
            const type = from[column];

            if (!utils.isEmpty(type)) {
                const typeDefinition = types.find((t) => t.name === type);

                if (typeDefinition == null) {
                    throw new Error(`Could not find component type '${type}'`);
                }

                const prefix = from[`${column} Prefix`];
                const suffix = from[`${column} Suffix`];

                let existingType = null;

                if (to.componentTypes) {
                    existingType = to.componentTypes.find((t) => t.componentTypeId === typeDefinition.componentTypeId);
                }

                if (existingType) {
                    existingType.codePrefix = utils.isEmpty(prefix) ? '' : prefix.trim();
                    existingType.codeSuffix = utils.isEmpty(suffix) ? '' : suffix.trim();
                } else {
                    associatedTypes.push({
                        componentTypeId: typeDefinition.componentTypeId,
                        codePrefix: utils.isEmpty(prefix) ? '' : prefix.trim(),
                        codeSuffix: utils.isEmpty(suffix) ? '' : suffix.trim(),
                    });
                }
            }
        }
    });

    if (associatedTypes.length > 0) {
        to.componentTypes = associatedTypes;
    }

    if (!utils.isEmpty(from.Code)) {
        to.code = utils.asString(from.Code);
    }

    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    updateTags(to, from);

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);

    return utils.isObjectDifferent(to, before);
}

/**
 * Populates API product object from a CSV source..
 */
async function updateProduct(to, from) {
    const before = utils.captureObject(to);

    if (!utils.isEmpty(from['Style Code'])) {
        to.styleCode = utils.asString(from['Style Code']);
    }

    if (!utils.isEmpty(from['Product Status'])) {
        to.productStatus = utils.asString(from['Product Status']);
    }

    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    if (!utils.isEmpty(from.Active)) {
        to.inactive = !utils.isTrue(from.Active);
    }

    if (!utils.isEmpty(from['Active From'])) {
        to.activeFrom = utils.parseDate(from['Active From']).toISOString();
    }

    updateTags(to, from);

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);

    if (!utils.isEmpty(from['Parent Products'])) {
        let parentStyles = from['Parent Products'];

        if (typeof parentStyles === 'string') {
            parentStyles = parentStyles.split(',').map((style) => style.trim()).filter((style) => style.length > 0);

            const uniqueStyles = [];

            parentStyles.forEach((style) => {
                if (!uniqueStyles.includes(style)) {
                    uniqueStyles.push(style);
                }
            });

            const uniqueIds = [];

            await utils.forEach(uniqueStyles, async (parentStyle) => {
                const parentProduct = await api.get(`/api/v1/product/style=${parentStyle}`).catch(() => {
                    throw new Error(`Could not find parent product '${parentStyle}'`);
                });

                console.log(`... found parent product ${parentStyle}`.dim.blue);

                uniqueIds.push(parentProduct.data.productId);
            });

            to.parentProducts = uniqueIds.map((id) => (to.parentProducts != null && to.parentProducts.find((parent) => parent.parentProductId === id)) || {
                parentProductId: id,
            });
        }
    }

    return utils.isObjectDifferent(to, before);
}

/**
 * Deletes generators associated with a placement.
 */
async function deletePlacementGenerators(to, deletedPlacements) {
    if (deletedPlacements.length > 0) {
        const codes = deletedPlacements.map((p) => (p.componentPlacement ? p.componentPlacement.code : null)).filter((x) => x);

        console.log(`... ... deleted placements ${codes.join(', ')}, removing generators`.yellow);

        // eslint-disable-next-line no-await-in-loop
        const packages = await api.getPackages(to);

        if (packages.componentPackages) {
            // eslint-disable-next-line no-await-in-loop
            await utils.forEach(packages.componentPackages, async (existingPackage) => {
                console.log(`... ... in package ${existingPackage.name}`.yellow);

                const { generators } = await api.getCachedGenerators(to, existingPackage.name);

                if (generators.generators) {
                    await utils.forEach(generators.generators, async (generator) => {
                        if (generator.parameters
                            && generator.parameters.componentPlacements
                            && generator.parameters.componentPlacements.length > 0) {
                            // There should be only one placement.
                            const placementId = generator.parameters.componentPlacements[0];

                            const deleted = deletedPlacements.some((p) => p.componentPlacementId === placementId);

                            if (deleted) {
                                console.log(`... ... ... generator with ${generator.generated} components`.yellow);

                                // eslint-disable-next-line max-len
                                await api.erase(`/api/v1/product/${to.productId}/version/${to.versionId}/package/${existingPackage.componentPackageId}/generator/${generator.componentGeneratorId}`, generator);

                                console.log('... ... ... ... removed a generator'.yellow);
                            }
                        }
                    });
                }
            });
        }
    }
}

/**
 * Populates API version object from a CSV source..
 */
async function updateVersion(to, from) {
    const before = utils.captureObject(to);

    const fromLength = Object.entries(from).length;

    if (!utils.isEmpty(from.Name)) {
        to.name = utils.asString(from.Name);
    }

    if (!utils.isEmpty(from.Description)) {
        to.description = utils.asString(from.Description);
    }

    to.customAttributesJson = utils.updateCustomAttributes(to.customAttributesJson, from);

    const groups = [];

    for (let groupIndex = 1; groupIndex < fromLength; groupIndex += 1) {
        const groupCode = from[`Group ${groupIndex} Code`];

        if (!utils.isEmpty(groupCode)) {
            let group = (to.componentGroups || []).find((g) => g.code === groupCode);

            if (group == null) {
                console.log(`... create a new group ${groupCode}`.green);

                group = {
                    temporaryId: utils.unique(),
                    code: utils.asString(groupCode),
                    customAttributesJson: [],
                    componentPlacements: [],
                };
            } else {
                console.log(`... group ${group.code} already exists`.blue);
            }

            groups.push(group);

            if (!utils.isEmpty(from[`Group ${groupIndex} Name`])) {
                group.name = utils.asString(from[`Group ${groupIndex} Name`]);
            }

            if (!utils.isEmpty(from[`Group ${groupIndex} Order Key`])) {
                group.explicitOrder = utils.asNumber(from[`Group ${groupIndex} Order Key`]);
            }

            group.customAttributesJson = utils.updateCustomAttributes(group.customAttributesJson, from, `Group ${groupIndex}`);

            const placements = [];

            for (let placementIndex = 1; placementIndex < fromLength; placementIndex += 1) {
                const placementPrefix = `Group ${groupIndex} Placement ${placementIndex}`;

                const placementCode = from[placementPrefix];

                if (!utils.isEmpty(placementCode)) {
                    // Make it serial.
                    // eslint-disable-next-line
                    const placement = await api.getPlacementByCode(placementCode);

                    if (placement == null) {
                        throw new Error(`Placement '${placementCode}' not found`);
                    }

                    let association = (group.componentPlacements || []).find((a) => a.componentPlacementId === placement.componentPlacementId);

                    if (association == null) {
                        console.log(`... ... creating placement association ${placementCode}`.green);

                        association = {
                            componentPlacementId: placement.componentPlacementId,
                        };
                    } else {
                        console.log(`... ... placement association ${placementCode} already exists`.blue);
                    }

                    placements.push(association);

                    if (!utils.isEmpty(from[`${placementPrefix} Hidden`])) {
                        association.hidden = utils.isTrue(from[`${placementPrefix} Hidden`]);
                    }

                    if (!utils.isEmpty(from[`${placementPrefix} Locked`])) {
                        association.locked = utils.isTrue(from[`${placementPrefix} Locked`]);
                    }

                    if (!utils.isEmpty(from[`${placementPrefix} Sync Key`])) {
                        association.synchronizationKey = utils.asString(from[`${placementPrefix} Sync Key`]);
                    }

                    if (!utils.isEmpty(from[`${placementPrefix} Quantity`])) {
                        association.quantityMultiplier = utils.asNumber(from[`${placementPrefix} Quantity`]);
                    }

                    association.customAttributesJson = utils.updateCustomAttributes(association.customAttributesJson, from, placementPrefix);
                }
            }

            if (group.componentPlacements) {
                const deletedPlacements = group.componentPlacements.filter((a) => !placements.some((p) => p.componentPlacementId === a.componentPlacementId));

                // eslint-disable-next-line no-await-in-loop
                await deletePlacementGenerators(to, deletedPlacements);
            }

            group.componentPlacements = placements;
        }
    }

    if (to.componentGroups != null && groups != null) {
        await utils.forEach(to.componentGroups, async (existingGroup) => {
            const matched = groups.find((g) => g.code === existingGroup.code);

            if (matched == null) {
                console.log(`... ... deleted group ${existingGroup.code}`.yellow);

                const deletedPlacements = existingGroup.componentPlacements;

                await deletePlacementGenerators(to, deletedPlacements);
            }
        });
    }

    to.componentGroups = groups;

    const views = [];

    for (let viewIndex = 1; viewIndex < fromLength; viewIndex += 1) {
        const viewPrefix = `View ${viewIndex}`;

        const viewCode = from[`${viewPrefix} Code`];

        if (!utils.isEmpty(viewCode)) {
            let view = (to.imageViews || []).find((v) => v.code === viewCode);

            if (view == null) {
                console.log(`... ... creating view ${viewCode}`.green);

                view = {
                    temporaryId: utils.unique(),
                    code: viewCode,
                    componentPlacements: [],
                    customAttributesJson: [],
                };
            } else {
                console.log(`... ... view ${viewCode} already exists`.green);
            }

            views.push(view);

            if (!utils.isEmpty(from[`${viewPrefix} Name`])) {
                view.name = utils.asString(from[`${viewPrefix} Name`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Hidden`])) {
                view.hidden = utils.isTrue(from[`${viewPrefix} Hidden`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Order Key`])) {
                view.explicitOrder = utils.asNumber(from[`${viewPrefix} Order Key`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Background`])) {
                view.backgroundComponent = utils.asString(from[`${viewPrefix} Background`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Angle`])) {
                view.viewType = 'Single';
                view.frameLow = utils.asNumber(from[`${viewPrefix} Angle`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Bake`])) {
                view.bakeInRecipe = utils.isTrue(from[`${viewPrefix} Bake`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Production Ready`])) {
                view.productionReady = utils.isTrue(from[`${viewPrefix} Production Ready`]);
            }

            if (!utils.isEmpty(from[`${viewPrefix} Only Selected`])) {
                view.onlySelectedPlacements = utils.isTrue(from[`${viewPrefix} Only Selected`]);
            }

            view.customAttributesJson = utils.updateCustomAttributes(view.customAttributesJson, from, viewPrefix);

            const viewPlacements = [];

            for (let placementIndex = 1; placementIndex < fromLength; placementIndex += 1) {
                const placementPrefix = `View ${viewIndex} Placement ${placementIndex}`;

                const placementCode = from[placementPrefix];

                if (!utils.isEmpty(placementCode)) {
                    // Make it serial.
                    // eslint-disable-next-line
                    const placement = await api.getPlacementByCode(placementCode);

                    if (placement == null) {
                        throw new Error(`Placement '${placementCode}' not found`);
                    }

                    let association = (view.componentPlacements || []).find((a) => a.componentPlacementId === placement.componentPlacementId);

                    if (association == null) {
                        console.log(`... ... creating view placement association ${placementCode}`.green);

                        association = {
                            componentPlacementId: placement.componentPlacementId,
                        };
                    } else {
                        console.log(`... ... view placement association ${placementCode} already exists`.blue);
                    }

                    viewPlacements.push(association);

                    if (!utils.isEmpty(from[`${placementPrefix} Hidden`])) {
                        association.hidden = utils.isTrue(from[`${placementPrefix} Hidden`]);
                    }

                    association.customAttributesJson = utils.updateCustomAttributes(association.customAttributesJson, from, placementPrefix);
                }
            }

            view.componentPlacements = viewPlacements;
        }
    }

    to.imageViews = views;

    return utils.isObjectDifferent(to, before);
}

/**
 * Creates or updates a definition.
 * @param  {Object} definition Definition object with fields as definited by the CSV structure.
 * @param  {Array} definitions A List of component definition definitions.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdateDefinition(definition, options) {
    const existingDefinitionsResponse = await api.get('/api/v1/componentoptiondefinition');

    const existingDefinition = existingDefinitionsResponse.data.find((existing) => existing.name === definition.Name);

    if (existingDefinition) {
        console.log(`Definition ${definition.Name} already exists`.blue);

        // Update the existing definition.
        await updateDefinition(existingDefinition, definition);

        if (options && options.configureDefinition) {
            await options.configureDefinition(existingDefinition, definition);
        }

        const result = await api.put(`/api/v1/componentoptiondefinition/${existingDefinition.componentOptionDefinitionId}`, existingDefinition);

        console.log('... saved'.green);

        return result;
    }

    console.log(`Creating new definition ${definition.Name}`.green);

    const newDefinition = {};

    await updateDefinition(newDefinition, definition);

    if (options && options.configureDefinition) {
        await options.configureDefinition(newDefinition, definition);
    }

    const result = await api.post('/api/v1/componentoptiondefinition/', newDefinition);

    console.log('... saved'.green);

    return result;
}

exports.createOrUpdateDefinition = createOrUpdateDefinition;

/**
 * Creates or updates an option.
 * @param  {Object} option Option object with fields as definited by the CSV structure.
 * @param  {Array} definitions A List of component option definitions.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdateOption(option, { definitions, options: allOptions }, options) {
    // Prevent chaining inside the method.
    let existingOptionResponse = allOptions.find((o) => o.code === option.Code);
    let existingOption;

    if (existingOptionResponse == null) {
        try {
            existingOptionResponse = await api.get(`/api/v1/componentoption/code=${option.Code}`);

            existingOption = existingOptionResponse.data;

            console.log(`Option ${option.Code} already exists (freshly loaded)`.blue);
        } catch (e) {
            // Nothing.
        }
    } else {
        existingOption = existingOptionResponse;

        console.log(`Option ${option.Code} already exists (in the preloaded list)`.blue);
    }

    if (existingOption) {
        // Update the existing option.
        let changed = (await updateOption(existingOption, option, definitions) || false);

        if (options && options.configureOption) {
            changed = changed || (await options.configureOption(existingOption, option) || false);
        }

        if (changed) {
            const result = await api.put(`/api/v1/componentoption/${existingOption.componentOptionId}`, existingOption);

            console.log('... saved'.green);

            return result;
        }

        console.log('... no changes'.dim.green);

        return existingOption;
    }

    console.log(`Creating new option ${option.Code}`.green);

    const newOption = {};

    await updateOption(newOption, option, definitions);

    if (options && options.configureOption) {
        await options.configureOption(newOption, option);
    }

    const result = await api.post('/api/v1/componentoption/', newOption);

    console.log('... saved'.green);

    return result;
}

exports.createOrUpdateOption = createOrUpdateOption;

/**
 * Creates or updates an type.
 * @param  {Object} type Option object with fields as definited by the CSV structure.
 * @param  {Array} definitions A List of component type definitions.
 * @param  {Object} types Process customization types.
 */
async function createOrUpdateType(type, definitions, types) {
    const existingTypeResponse = await api.get('/api/v1/componenttype');

    const existingType = existingTypeResponse.data.find((existing) => existing.name === type.Name);

    if (existingType) {
        console.log(`Type ${type.Name} already exists`.blue);

        // Update the existing type.
        let changed = (await updateType(existingType, type, definitions) || false);

        if (types && types.configureType) {
            changed = changed || (await types.configureType(existingType, type) || false);
        }

        if (changed) {
            const result = await api.put(`/api/v1/componenttype/${existingType.componentTypeId}`, existingType);

            console.log('... saved'.green);

            return result;
        }

        console.log('... no changes'.dim.green);

        return existingType;
    }

    console.log(`Creating new type ${type.Name}`.green);

    const newType = {};

    await updateType(newType, type, definitions);

    if (types && types.configureType) {
        await types.configureType(newType, type);
    }

    const result = await api.post('/api/v1/componenttype/', newType);

    console.log('... saved'.green);

    return result;
}

exports.createOrUpdateType = createOrUpdateType;

/**
 * Creates or updates a placement.
 * @param  {Object} placement Placment object with fields as definited by the CSV structure.
 * @param  {Array} types A List of component types.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdatePlacement(placement, types, options) {
    // Prevent chaining inside the method.
    const existingPlacement = await api.getPlacementByCode(placement.Code);

    if (existingPlacement) {
        console.log(`Placement ${placement.Name} already exists`.blue);

        // Update the existing placement.
        let changed = await updatePlacement(existingPlacement, placement, types) || false;

        if (options && options.configurePlacement) {
            changed = changed || (await options.configurePlacement(existingPlacement, placement) || false);
        }

        if (changed) {
            const result = await api.put(`/api/v1/componentplacement/${existingPlacement.componentPlacementId}`, existingPlacement);

            console.log('... saved'.green);

            return result;
        }

        console.log('... no changes'.dim.green);

        return existingPlacement;
    }

    console.log(`Creating new placement ${placement.Code}`.green);

    const newPlacement = {};

    await updatePlacement(newPlacement, placement, types);

    if (options && options.configurePlacement) {
        await options.configurePlacement(newPlacement, placement);
    }

    const result = await api.post('/api/v1/componentplacement/', newPlacement);

    console.log('... saved'.green);

    return result;
}

exports.createOrUpdatePlacement = createOrUpdatePlacement;

/**
 * Creates or updates a product.
 * @param  {Object} product Product object with fields as definited by the CSV structure.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdateProduct(product, options) {
    let existingProductResponse = null;
    try {
        existingProductResponse = await api.get(`/api/v1/product/style=${product['Style Code']}`);
    } catch (e) {
        // Nothing;
    }

    if (existingProductResponse) {
        console.log(`Product ${product['Style Code']} already exists`.blue);

        const existingProduct = existingProductResponse.data;

        // Update the existing product.
        let changed = await updateProduct(existingProduct, product) || false;

        if (options && options.configureProduct) {
            changed = changed || (await options.configureProduct(existingProduct, product) || false);
        }

        if (changed) {
            const result = await api.put(`/api/v1/product/${existingProduct.productId}`, existingProduct);

            console.log('... saved'.green);

            return result;
        }

        console.log('... no changes'.dim.green);

        return existingProduct;
    }

    console.log(`Creating new product ${product['Style Code']}`.green);

    const newProduct = {
        // Clear out image format to avoid default options;
        imageNameFormat: [],
        // Product status is required field, but don't need it in the source data.
        productStatus: 'Development',
    };

    await updateProduct(newProduct, product);

    if (options && options.configureProduct) {
        await options.configureProduct(newProduct, product);
    }

    const result = await api.post('/api/v1/product/', newProduct);

    console.log('... saved'.green);

    return result;
}

exports.createOrUpdateProduct = createOrUpdateProduct;

/**
 * Creates or updates a product version.
 * @param  {Object} version Version object with fields as definited by the CSV structure.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdateProductVersion(version, options) {
    // The latest version should always exist.
    const existingVersion = await api.getLatestVersionByStyle(version['Style Code']);

    console.log(`Updating product version ${version['Style Code']} v${existingVersion.code}`.blue);

    // Update the existing product.
    let changed = await updateVersion(existingVersion, version) || false;

    if (options && options.configureVersion) {
        changed = changed || (await options.configureVersion(existingVersion, version) || false);
    }

    if (changed) {
        const result = await api.put(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}`, existingVersion);

        console.log('... saved'.green);

        return result;
    }

    console.log('... no changes'.dim.green);

    return existingVersion;
}

exports.createOrUpdateProductVersion = createOrUpdateProductVersion;

/**
 * Generates an expanded list of options.
 */
function expandOptions(list) {
    const options = [];
    const indices = [];

    for (let listIndex = 0; listIndex < list.length; listIndex += 1) {
        indices.push(0);
    }

    for (; ;) {
        const ids = [];

        for (let listIndex = 0; listIndex < list.length; listIndex += 1) {
            ids.push(`D:${list[listIndex].componentOptionDefinitionId}`);
            ids.push(`O:${list[listIndex].componentOptionIds[indices[listIndex]]}`);
        }

        options.push(ids.join(','));

        let incrementAt = list.length - 1;

        indices[incrementAt] += 1;
        for (; ;) {
            if (!list[incrementAt]) {
                break;
            }

            if (indices[incrementAt] >= list[incrementAt].componentOptionIds.length) {
                indices[incrementAt] = 0;
                incrementAt -= 1;
                if (incrementAt < 0) {
                    break;
                }
                indices[incrementAt] += 1;
            } else {
                break;
            }
        }

        if (incrementAt < 0) {
            break;
        }
    }

    return options;
}

/**
 * Expands multiple options arrays.
 */
function expandOptionsDeep(listOfLists) {
    return [].concat(...listOfLists.map((list) => expandOptions(list)));
}

/**
 * Checks arrays for equality.
 */
function areArraysEqual(a, b) {
    if (a.length !== b.length) {
        return false;
    }

    for (let i = 0; i < a.length; i += 1) {
        if (a[i] !== b[i]) {
            return false;
        }
    }

    return true;
}

/**
 * Creates or updates a generator.
 * @param  {Object} generator Generator object with fields as definited by the CSV structure.
 * @param  {Object} options Process customization options.
 */
async function createOrUpdateGenerator(generator) {
    // The latest version should always exist.
    const existingVersion = await api.getCachedLatestVersionByStyle(generator.styleCode);

    console.log(`Updating product version ${generator.styleCode} v${existingVersion.code}`.blue);

    const { generatorPackage, generators } = await api.getCachedGenerators(existingVersion, generator.packageName);

    // Check if generators for the placement already exist.
    const expandedExistingOptions = [];
    const expandedIncomingOptions = expandOptionsDeep(generator.generators);

    generators.generators.forEach((existingGenerator) => {
        if (existingGenerator.parameters && existingGenerator.parameters.componentPlacements) {
            const existingPlacementId = existingGenerator.parameters.componentPlacements[0];

            if (existingPlacementId === generator.placementId) {
                expandedExistingOptions.push(...expandOptions(existingGenerator.parameters.enabledOptions));
            }
        }
    });

    // Do existing and incoming arrays match?
    expandedIncomingOptions.sort();
    expandedExistingOptions.sort();

    if (areArraysEqual(expandedExistingOptions, expandedIncomingOptions)) {
        console.log('... options are already the same'.dim.green);
    } else {
        console.log('... options do not match'.green);

        const toDelete = [];

        // Collect old generators.
        generators.generators.forEach((existingGenerator) => {
            if (existingGenerator.parameters && existingGenerator.parameters.componentPlacements) {
                const existingPlacementId = existingGenerator.parameters.componentPlacements[0];

                if (existingPlacementId === generator.placementId) {
                    toDelete.push(existingGenerator);
                }
            }
        });

        // If only a single generator is involved, update it rather than delete and rebuild.
        if (toDelete.length === 1 && generator.generators.length === 1) {
            // Rebuild generators.
            await utils.forEach(generator.generators, async (incomingGenerator) => {
                const payload = {
                    etag: toDelete[0].etag,
                    parameters: {
                        componentPlacements: [generator.placementId],
                        enabledOptions: [],
                    },
                };

                incomingGenerator.forEach((enabledOption) => {
                    payload.parameters.enabledOptions.push({
                        componentOptionDefinitionId: enabledOption.componentOptionDefinitionId,
                        componentOptionIds: enabledOption.componentOptionIds,
                    });
                });

                if (payload.parameters.enabledOptions.length === 0) {
                    console.log('... ... no options to assign'.yellow);
                } else {
                    // eslint-disable-next-line max-len
                    await api.put(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package/${generatorPackage.componentPackageId}/generator/${toDelete[0].componentGeneratorId}`, payload);

                    console.log('... ... updated a generator'.green);
                }
            });

            api.clearCachedGenerators(existingVersion, generator.packageName);
        } else {
            // Delete old generators.
            await utils.forEach(toDelete, async (existingGenerator) => {
                // eslint-disable-next-line max-len
                await api.erase(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package/${generatorPackage.componentPackageId}/generator/${existingGenerator.componentGeneratorId}`,
                    existingGenerator);

                console.log('... removed a generator'.yellow);
            });

            // Rebuild generators.
            await utils.forEach(generator.generators, async (incomingGenerator) => {
                const payload = {
                    parameters: {
                        componentPlacements: [generator.placementId],
                        enabledOptions: [],
                    },
                };

                incomingGenerator.forEach((enabledOption) => {
                    payload.parameters.enabledOptions.push({
                        componentOptionDefinitionId: enabledOption.componentOptionDefinitionId,
                        componentOptionIds: enabledOption.componentOptionIds,
                    });
                });

                if (payload.parameters.enabledOptions.length === 0) {
                    console.log('... ... no options to assign'.yellow);
                } else {
                    // eslint-disable-next-line max-len
                    await api.post(`/api/v1/product/${existingVersion.productId}/version/${existingVersion.versionId}/package/${generatorPackage.componentPackageId}/generator`, payload);

                    console.log('... ... saved a generator'.green);
                }
            });

            api.clearCachedGenerators(existingVersion, generator.packageName);
        }

        console.log('... updated'.green);
    }

    return null;
}

exports.createOrUpdateGenerator = createOrUpdateGenerator;
