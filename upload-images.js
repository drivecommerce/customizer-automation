#!/usr/bin/env node

const colors = require('colors');

colors.enabled = true;

const fs = require('fs');
const FormData = require('form-data');
const axios = require('axios');

const argv = require('yargs')
.options({
    input: {
        describe: 'provide a path to source CSV or XLSX file',
        demandOption: true,
    },
    target: {
        describe: 'target account URL',
    },
    resetwith: {
        describe: 'request a cache flush after the upload using the provided token',
    },
})
.help()
.argv;

const utils = require('./utils.js');

/**
 * Uploads images.
 * @param  {Array} images Array of image records.
 */
async function processImage(images) {
    await utils.forEach(images, async (image) => {
        const imageName = image['Image Name'];
        const from = image.From;

        console.log(`Uploading ${imageName} from ${from}`.grey);

        if (!utils.isEmpty(imageName) && !utils.isEmpty(from)) {
            const data = new FormData();

            if (from.startsWith('.') || from.startsWith('/') || from.startsWith('\\')) {
                try {
                    const fileContent = fs.readFileSync(from);

                    data.append('copy-from', fileContent.toString('base64'));
                } catch (e) {
                    console.log(`Could not process picture: ${JSON.stringify(e)}`.red);

                    if (!argv.continueonerror) {
                        throw new Error(JSON.stringify(e));
                    }

                    return;
                }
            } else {
                data.append('copy-from', from);
            }

            data.append('with-name', imageName);

            if (!utils.isEmpty(image.Fit)) {
                data.append('fit', utils.asString(image.Fit));
            }

            if (!utils.isEmpty(image['Fit Width'])) {
                data.append('fit-width', utils.asNumber(image['Fit Width']));
            }

            if (!utils.isEmpty(image['Fit Height'])) {
                data.append('fit-height', utils.asNumber(image['Fit Height']));
            }

            if (!utils.isEmpty(image.Rotate)) {
                data.append('rotate', utils.asNumber(image.Rotate));
            }

            if (!utils.isEmpty(image['Crop X'])) {
                data.append('crop-x', utils.asNumber(image['Crop X']));
            }

            if (!utils.isEmpty(image['Crop Y'])) {
                data.append('crop-y', utils.asNumber(image['Crop Y']));
            }

            if (!utils.isEmpty(image['Crop Width'])) {
                data.append('crop-width', utils.asNumber(image['Crop Width']));
            }

            if (!utils.isEmpty(image['Crop Height'])) {
                data.append('crop-height', utils.asNumber(image['Crop Height']));
            }

            let imageLink = argv.target;

            if (!argv.target.endsWith('/')) {
                imageLink += '/';
            }

            imageLink += imageName;

            let uploaded = true;

            await axios.request({
                method: 'HEAD',
                url: imageLink,
            }).catch(() => {
                uploaded = false;
            });

            if (uploaded) {
                console.log(`... alreaded loaded ${imageName} as ${imageLink}`.green);
            }

            if (argv.forceupload) {
                uploaded = false;
            }

            if (!uploaded) {
                const url = argv.target.endsWith('/') ? `${argv.target}upload` : `${argv.target}/upload`;

                const result = await axios.request({
                    method: 'POST',
                    url,
                    data,
                    headers: data.getHeaders(),
                }).catch((error) => {
                    if (error.response) {
                        console.log(`Could not process picture: ${JSON.stringify(error.response.data)}`.red);
                    } else {
                        console.log(`Could not process picture: ${JSON.stringify(error)}`.red);
                    }

                    if (!argv.continueonerror) {
                        if (error.response) {
                            throw new Error(JSON.stringify(error.response.data));
                        } else {
                            throw new Error(JSON.stringify(error));
                        }
                    }
                });

                if (result) {
                    console.log(`... uploaded ${result.data.name} as ${result.data.imageUrl}`.green);
                }
            }
        }
    });

    if (!utils.isEmpty(argv.resetwith)) {
        let url = argv.target.replace('/image/', '/control/');

        url = url.endsWith('/') ? `${url}reset` : `${url}/reset`;

        console.log('Resetting the cache...'.green);

        await axios.post(url, {
            key: argv.resetwith,
        }).catch((error) => {
            throw new Error(JSON.stringify(error.response.data));
        });
    }

    done();
}

(async function runner() {
    const data = await run().catch((error) => fail(error));

    await processImage(data).catch((error) => fail(error));
}());

/*
 *********************************************************************************************************************
 Standard-ish component loaders, flow controllers, etc.
 *********************************************************************************************************************
*/

/**
 * Completes the importer script.
 */
function done() {
    console.log('Done'.brightWhite);
    process.exit(0);
}

/**
 * Completes the script with an error state.
 */
function fail(error) {
    if (error) {
        console.log('Failed:'.red);
        console.log(error);
    } else {
        console.log('Failed with unknown reason'.red);
    }
    process.exit(1);
}

/**
 * Standard importer setup and source loader.
 */
async function run() {
    // Load the source file.
    return utils.loadSource(argv.input);
}
