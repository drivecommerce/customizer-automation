#!/usr/bin/env node

// Package importer may be a little hard to customize, however, there is typically not much
// that needs to be tweaked in the option generation process.

const colors = require('colors');

colors.enabled = true;

const argv = require('./yargs');
const api = require('./api.js');
const utils = require('./utils.js');
const objects = require('./objects.js');

// Imports a list of packages from a CSV or XLSX file.

async function processGenerator(packages, context) {
    // Aggregate generators by Style Code + Package + Placement.
    const combinedPackages = [];

    packages.forEach((row) => {
        const styleCode = utils.asString(row['Style Code']);
        const packageName = utils.asString(row.Package);
        const placementCode = utils.asString(row.Placement);

        if (!utils.isEmpty(styleCode) && !utils.isEmpty(packageName) && !utils.isEmpty(placementCode)) {
            const placement = context.placements.find((checkPlacement) => checkPlacement.code === placementCode);

            if (placement == null) {
                throw new Error(`Could not find '${placementCode}' placement`);
            }

            const generators = [];

            for (let definitionIndex = 1; ; definitionIndex += 1) {
                const definitionName = utils.asString(row[`Definition ${definitionIndex}`]);
                const options = utils.asString(row[`Definition ${definitionIndex} Options`]);

                if (!utils.isEmpty(definitionName) && !utils.isEmpty(options)) {
                    const definition = context.definitions.find((checkDefinition) => checkDefinition.name === definitionName);

                    if (definition == null) {
                        throw new Error(`Could not find definition '${definitionName}'`);
                    }

                    const optionsCleaned = options.split(',')
                    .map((optionCode) => optionCode.trim())
                    .filter((optionCode) => optionCode.length > 0)
                    .map((optionCode) => {
                        const option = context.options.find((checkOption) => checkOption.code === optionCode);

                        if (option == null) {
                            throw new Error(`Could not find option '${optionCode}'`);
                        }

                        return {
                            id: option.componentOptionId,
                            code: option.code,
                        };
                    });

                    if (optionsCleaned.length > 0) {
                        generators.push({
                            definitionName,
                            options: optionsCleaned,
                            componentOptionDefinitionId: definition.componentOptionDefinitionId,
                            componentOptionIds: optionsCleaned.map((option) => option.id),
                        });
                    }
                } else {
                    break;
                }
            }

            let record = combinedPackages.find((combined) => combined.styleCode === styleCode
                && combined.packageName === packageName
                && combined.placementId === placement.componentPlacementId);

            if (record == null) {
                record = {
                    styleCode,
                    packageName,
                    placementCode: placement.code,
                    placementId: placement.componentPlacementId,
                    generators: [],
                };

                combinedPackages.push(record);
            }

            record.generators.push(generators);
        }
    });

    await utils.forEach(combinedPackages, async (generators) => {
        console.log(`Evaluating ${generators.styleCode} generators for ${generators.placementCode}`.grey);

        await objects.createOrUpdateGenerator(generators, context, {
            configureGenerator: async (/* apiType, fromType */) => {
                // Adjust API generators object if necessary.
            },
        });
    });

    done();
}

(async function runner() {
    const data = await run().catch((error) => fail(error));

    const definitions = (await api.get('/api/v1/componentoptiondefinition')).data;
    const options = (await api.get('/api/v1/componentoption')).data;
    const placements = (await api.get('/api/v1/componentplacement')).data;

    await processGenerator(data, {
        definitions,
        options,
        placements,
    }).catch((error) => fail(error));
}());


/*
 *********************************************************************************************************************
 Standard-ish component loaders, flow controllers, etc.
 *********************************************************************************************************************
*/

/**
 * Completes the importer script.
 */
function done() {
    console.log('Done'.brightWhite);
    process.exit(0);
}

/**
 * Completes the script with an error state.
 */
function fail(error) {
    if (error) {
        console.log('Failed:'.red);
        console.log(error);
    } else {
        console.log('Failed with unknown reason'.red);
    }
    process.exit(1);
}


/**
 * Standard importer setup and source loader.
 */
async function run() {
    api.authorize(argv.apikey, argv.apiroot);

    // Load the source file.
    return utils.loadSource(argv.input);
}
