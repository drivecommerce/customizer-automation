// Most import files are following the same structure and the same set of initial parameters.

const argv = require('yargs')
.options({
    input: {
        describe: 'provide a path to source CSV or XLSX file',
        demandOption: true,
    },
    apikey: {
        describe: 'authorization API key',
        demandOption: true,
    },
    apiroot: {
        describe: 'API root URL',
    },
    dateformat: {
        description: 'optional date format',
    },
})
.help()
.argv;

module.exports = argv;
