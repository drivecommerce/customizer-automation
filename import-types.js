#!/usr/bin/env node

const colors = require('colors');

colors.enabled = true;

const argv = require('./yargs');
const api = require('./api.js');
const utils = require('./utils.js');
const objects = require('./objects.js');

// Imports a list of component types from a CSV or XLSX file.

async function processType(types, context) {
    await utils.forEach(types, async (type) => {
        if (type != null && !utils.isEmpty(type.Name)) {
            console.log(`Evaluating ${type.Name}`.grey);

            await objects.createOrUpdateType(type, context.definitions, {
                configureType: async (/* apiType, fromType */) => {
                    // Adjust API type object if necessary.
                },
            });
        }
    });

    done();
}

(async function runner() {
    const data = await run().catch((error) => fail(error));

    const definitions = await loadDefinitions();

    await processType(data, {
        definitions,
    }).catch((error) => fail(error));
}());

/*
 *********************************************************************************************************************
 Standard-ish component loaders, flow controllers, etc.
 *********************************************************************************************************************
*/

/**
 * Completes the importer script.
 */
function done() {
    console.log('Done'.brightWhite);
    process.exit(0);
}

/**
 * Completes the script with an error state.
 */
function fail(error) {
    if (error) {
        console.log('Failed:'.red);
        console.log(error);
    } else {
        console.log('Failed with unknown reason'.red);
    }
    process.exit(1);
}

/**
 * Load component option definitions.
 */
async function loadDefinitions() {
    // Load all component option definitions.
    const definitions = await api.get('/api/v1/componentoptiondefinition');

    console.log(`Loaded ${definitions.data.length} component option definitions`.grey);

    return definitions.data;
}

/**
 * Standard importer setup and source loader.
 */
async function run() {
    api.authorize(argv.apikey, argv.apiroot);

    // Load the source file.
    return utils.loadSource(argv.input);
}
