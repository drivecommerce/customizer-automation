#!/usr/bin/env node

const colors = require('colors');

colors.enabled = true;

const argv = require('./yargs');
const api = require('./api.js');
const utils = require('./utils.js');
const objects = require('./objects.js');

// Imports a list of options from a CSV or XLSX file.

async function processPlacement(placements, context) {
    await utils.forEach(placements, async (placement) => {
        if (placement != null && !utils.isEmpty(placement.Name)) {
            console.log(`Evaluating ${placement.Name}`.grey);

            // We want serial execution
            // eslint-disable-next-line no-await-in-loop
            await objects.createOrUpdatePlacement(placement, context.types, {
                configurePlacement: async (/* apiOption, fromOption */) => {
                    // Adjust API placement object if necessary.
                },
            });
        }
    });

    done();
}

(async function runner() {
    const data = await run().catch((error) => fail(error));

    const types = await loadTypes();

    await processPlacement(data, {
        types,
    }).catch((error) => fail(error));
}());

/*
 *********************************************************************************************************************
 Standard-ish component loaders, flow controllers, etc.
 *********************************************************************************************************************
*/

/**
 * Completes the importer script.
 */
function done() {
    console.log('Done'.brightWhite);
    process.exit(0);
}

/**
 * Completes the script with an error state.
 */
function fail(error) {
    if (error) {
        console.log('Failed:'.red);
        console.log(error);
    } else {
        console.log('Failed with unknown reason'.red);
    }
    process.exit(1);
}

/**
 * Load component option definitions.
 */
async function loadTypes() {
    // Load all component option definitions.
    const types = await api.get('/api/v1/componenttype');

    console.log(`Loaded ${types.data.length} component types`.grey);

    return types.data;
}

/**
 * Standard importer setup and source loader.
 */
async function run() {
    api.authorize(argv.apikey, argv.apiroot);

    // Load the source file.
    return utils.loadSource(argv.input);
}
