#!/usr/bin/env node

const colors = require('colors');

colors.enabled = true;

const argv = require('yargs')
.options({
    apikey: {
        describe: 'authorization API key',
        demandOption: true,
    },
    apiroot: {
        describe: 'API root URL',
    },
})
.help()
.argv;
const api = require('./api.js');

(async function runner() {
    console.log('Resetting the blueprint cache...'.green);

    api.authorize(argv.apikey, argv.apiroot);

    await api.flushCache();

    console.log('Done'.brightWhite);
    process.exit(0);
}());
